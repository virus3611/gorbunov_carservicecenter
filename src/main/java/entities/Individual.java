package entities;

import javax.persistence.Entity;

@Entity
public class Individual extends Customer {

    private String passportNo;

    public Individual() {
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }
}
