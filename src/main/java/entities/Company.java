package entities;

import javax.persistence.Entity;

@Entity
public class Company extends Customer {

    private String inn;

    public Company() {
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }
}
